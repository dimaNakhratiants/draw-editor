import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by HP on 09.02.2017.
 */
public class MenuActions {

    public static void saveAs(JMenuItem item) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.showSaveDialog(item);
        File file = fileChooser.getSelectedFile();
        try {
            ImageIO.write((RenderedImage) DrawEditor.getPaintSurface().getImage(), "PNG", file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void newFile(JMenuItem item) {
        showDialog(item);
        DrawEditor.getPaintSurface().clear();
    }

    public static void quit(JMenuItem item){
        showDialog(item);
        System.exit(0);
    }

    public static void open(JMenuItem item){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.showOpenDialog(item);
        File file = fileChooser.getSelectedFile();
        try {
            DrawEditor.getPaintSurface().loadFromImage(ImageIO.read(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void showDialog(JMenuItem item){
        if (!DrawEditor.getPaintSurface().isEmpty()) {
            int dialogResult = JOptionPane.showConfirmDialog(null, "Would You Like to Save your previous image?", "Warning", 1);
            if (dialogResult == 0)
                saveAs(item);
        }
    }
}
