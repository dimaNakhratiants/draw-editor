package figures;

import java.awt.*;
import java.awt.geom.Line2D;

/**
 * Created by HP on 09.02.2017.
 */
public class Line extends Figure {

    private static Line sLine;

    public static Line getInstance(){
        if (sLine == null) sLine = new Line();
        return sLine;
    }

    @Override
    public Shape draw(int x1, int y1, int x2, int y2) {
        return new Line2D.Float(x1, y1, x2, y2);
    }
}
