package figures;

import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * Created by HP on 09.02.2017.
 */
public class Rectangle extends Figure {

    private static Rectangle sRectangle;

    public static Rectangle getInstance(){
        if (sRectangle == null) sRectangle = new Rectangle();
        return sRectangle;
    }

    @Override
    public Shape draw(int x1, int y1, int x2, int y2) {
        return new Rectangle2D.Float(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x1 - x2), Math.abs(y1 - y2));
    }
}
