package figures;

import javafx.scene.shape.*;

import java.awt.*;
import java.awt.Shape;

/**
 * Created by Dima on 09.02.2017.
 */
public class Brush extends Line {

    private static Brush sBrush;

    public Brush() {
        setDrawMode(BRUSH);
    }

    public static Brush getInstance(){
        if (sBrush == null) sBrush = new Brush();
        return sBrush;
    }



}
