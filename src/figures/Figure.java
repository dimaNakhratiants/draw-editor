package figures;

import java.awt.*;

/**
 * Created by HP on 09.02.2017.
 */
public abstract class Figure {

    public static final int BRUSH = 1;
    public static final int FIGURE = 2;

    private int mDrawMode = FIGURE;

    protected void setDrawMode(int drawMode) {
        mDrawMode = drawMode;
    }

    public int getDrawMode() {
        return mDrawMode;
    }

    public abstract Shape draw(int x1, int y1, int x2, int y2);

}
