package figures;

import java.awt.*;
import java.awt.geom.Ellipse2D;

/**
 * Created by HP on 08.02.2017.
 */
public class Circle extends Figure {

    private static Circle sCircle;

    public static Circle getInstance(){
        if (sCircle == null) sCircle = new Circle();
        return sCircle;
    }

    @Override
    public Shape draw(int x1, int y1, int x2, int y2) {
        return new Ellipse2D.Float(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x1 - x2), Math.abs(y1 - y2));
    }
}
