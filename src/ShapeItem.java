import java.awt.*;
import java.util.ArrayList;

/**
 * Created by HP on 09.02.2017.
 */
public class ShapeItem {
    private ArrayList<Shape> mShapes;
    private Color mColor;
    private Color mFillColorColor;
    private int mStroke = 1;
    private float mAlpha = 1f;


    public ShapeItem(Shape shape, Color color, Color fillColor, int stroke, float alpha) {
        mShapes = new ArrayList<>(1);
        mShapes.add(shape);
        mColor = color;
        mFillColorColor = fillColor;
        mStroke = stroke;
        mAlpha = alpha;
    }

    public ShapeItem(ArrayList<Shape> shapes, Color color, Color fillColor, int stroke, float alpha) {
        mShapes = shapes;
        mColor = color;
        mFillColorColor = fillColor;
        mStroke = stroke;
        mAlpha = alpha;
    }


    public ShapeItem(ArrayList<Shape> shapes, Color color, int stroke, float alpha) {
        this(shapes, color, color, stroke, alpha);
    }

    public ShapeItem(Shape newFigure, Color color, int stroke, float alpha) {
        this(newFigure, color, color, stroke, alpha);
    }

    public int getStroke() {
        return mStroke;
    }

    public void setStroke(int stroke) {
        mStroke = stroke;
    }

    public Color getColor() {
        return mColor;
    }

    public ArrayList<Shape> getShape() {
        return mShapes;
    }

    public Color getFillColor() {
        return mFillColorColor;
    }

    public float getAlpha() {
        return mAlpha;
    }

    public void setAlpha(float alpha) {
        mAlpha = alpha;
    }
}
