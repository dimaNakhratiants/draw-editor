import javax.swing.*;
import java.awt.*;

/**
 * Created by HP on 09.02.2017.
 */
public class ColorChooserButton extends JButton {

    public static final int BORDER = 1;
    public static final int FILL = 2;
    private Color mCurrent;


    public ColorChooserButton(Color color, String title, int type) {
        super(title);
        addActionListener(arg0 -> {

            switch (type) {
                case BORDER:
                    Color newColor = JColorChooser.showDialog(null, "Choose border color", mCurrent);
                    ParamPicker.setColor(newColor);
                    break;
                case FILL:
                    Color newFillColor = JColorChooser.showDialog(null, "Choose fill color", mCurrent);
                    ParamPicker.setFillColor(newFillColor);
            }
        });
    }
}