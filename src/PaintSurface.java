import figures.Figure;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;

public class PaintSurface extends JComponent {


    private ArrayList<ShapeItem> mShapes = new ArrayList<>();
    private ArrayList<ShapeItem> mCurrentBrush = new ArrayList<>();
    private ArrayList<ShapeItem> mRemovedShapes = new ArrayList<>();
    private ArrayList<Shape> mFromBrush;

    private Point mStartDrag, mEndDrag, mOldPosition;

    private Image mImage;
    private Figure figure;
    private Graphics2D mGraphics;

    public PaintSurface() {
        this.setFocusable(true);
        setBackground(Color.white);
        repaint();
        this.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (figure.getDrawMode() == Figure.BRUSH) {
                    Shape newFigure = figure.draw(e.getX(), e.getY(), e.getX(), e.getY());
                    mFromBrush = new ArrayList<>();
                    mFromBrush.add(newFigure);
                }
                mStartDrag = new Point(e.getX(), e.getY());
                mEndDrag = mStartDrag;
                mOldPosition = mStartDrag;
                repaint();
            }

            public void mouseReleased(MouseEvent e) {
                if (figure.getDrawMode() == Figure.BRUSH) {
                    mCurrentBrush = new ArrayList<>();
                    drawBrush();
                } else {
                    drawFigure(e);
                }
                mRemovedShapes = new ArrayList<>();
            }
        });

        this.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                mEndDrag = new Point(e.getX(), e.getY());
                if (figure.getDrawMode() == Figure.BRUSH) {
                    drawCurrentBrush(e);
                }
                repaint();
            }
        });
    }

    private void drawFigure(MouseEvent e) {
        Shape newFigure = figure.draw(mStartDrag.x, mStartDrag.y, e.getX(), e.getY());
        mShapes.add(new ShapeItem(newFigure, ParamPicker.getColor(),
                ParamPicker.getFillColor(), ParamPicker.getStroke(),ParamPicker.getAlpha()));
        mStartDrag = null;
        mEndDrag = null;
        repaint();
    }

    private void drawBrush() {
        mShapes.add(new ShapeItem(mFromBrush, ParamPicker.getColor(),
                ParamPicker.getStroke(),ParamPicker.getAlpha()));
        mStartDrag = null;
        mEndDrag = null;
        repaint();
    }

    private void drawCurrentBrush(MouseEvent e) {
        Shape newFigure = figure.draw((int) mOldPosition.getX(), (int) mOldPosition.getY(), e.getX(), e.getY());
        mFromBrush.add(newFigure);
        mCurrentBrush.add(new ShapeItem(newFigure, ParamPicker.getColor(),
                ParamPicker.getStroke(),ParamPicker.getAlpha()));
        mOldPosition = mEndDrag;
    }

    public void setFigure(Figure figure) {
        this.figure = figure;
    }

    public void paint(Graphics g) {
        mGraphics = (Graphics2D) g;
        mGraphics.setColor(Color.WHITE);
        mGraphics.fillRect(0,0,getWidth(),getHeight());
        mGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        if (mImage != null) {
            mGraphics.drawImage(mImage, 0, 0, null);
        }
        for (ShapeItem s : mShapes) {
            mGraphics.setStroke(new BasicStroke(s.getStroke()));
            s.getShape().forEach(
                    shape -> {
                        mGraphics.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,s.getAlpha()));
                        mGraphics.setPaint(s.getFillColor());
                        mGraphics.fill(shape);
                        mGraphics.setPaint(s.getColor());
                        mGraphics.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,1f));
                        mGraphics.draw(shape);
                    }
            );
        }

        for (ShapeItem s : mCurrentBrush) {
            mGraphics.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,1f));
            mGraphics.setStroke(new BasicStroke(s.getStroke()));
            mGraphics.setPaint(s.getColor());
            s.getShape().forEach(mGraphics::draw);
        }
        if (mStartDrag != null && mEndDrag != null && figure.getDrawMode() == Figure.FIGURE) {
            Shape newFigure = figure.draw(mStartDrag.x, mStartDrag.y, mEndDrag.x, mEndDrag.y);
            mGraphics.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, ParamPicker.getAlpha()));
            mGraphics.setStroke(new BasicStroke(ParamPicker.getStroke()));
            mGraphics.setPaint(ParamPicker.getFillColor());
            mGraphics.fill(newFigure);
            mGraphics.setPaint(ParamPicker.getColor());
            mGraphics.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,1f));
            mGraphics.draw(newFigure);
        }
    }

    public void clear() {
        mShapes.clear();
        repaint();
    }

    public void undo() {
        if (mShapes.size() > 0) {
            int index = mShapes.size() - 1;
            mRemovedShapes.add(mShapes.get(index));
            mShapes.remove(index);
            repaint();
        } else if (mImage != null) mImage = null;
        repaint();
    }

    public void redo() {
        if (mRemovedShapes.size() > 0) {
            int index = mRemovedShapes.size() - 1;
            mShapes.add(mRemovedShapes.get(index));
            mRemovedShapes.remove(index);
        }
        repaint();
    }

    public Image getImage() {
        Image image = createImage(getSize().width, getSize().height);
        Graphics graphics = image.getGraphics();
        paint(graphics);
        return image;
    }

    public void loadFromImage(Image image) {
        mImage = image;
        getGraphics().drawImage(image, 0, 0, null);
    }

    public boolean isEmpty() {
        return mShapes.size() == 0;
    }

}