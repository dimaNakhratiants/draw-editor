import figures.*;
import figures.Rectangle;

import java.awt.*;
import java.awt.event.*;
import java.util.Hashtable;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class DrawEditor {

    public static final int MINIMUM_STROKE = 1;
    public static final int MAXIMUM_STROKE = 20;


    public static final int MINIMUM_ALPHA = 1;
    public static final int MAXIMUM_ALPHA = 100;

    private static PaintSurface sPaintSurface;
    private static JFrame sMainFrame;
    private Container mContainer;
    private static JButton borderColor;
    private static JButton fillColor;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        new DrawEditor().show();
    }

    public void show() {

        sMainFrame = new JFrame("Draw Editor");
        sMainFrame.setExtendedState(sMainFrame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
        sMainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setupMenu();

        mContainer = sMainFrame.getContentPane();
        mContainer.setLayout(new BorderLayout());

        sPaintSurface = new PaintSurface();
        sPaintSurface.setFigure(new Brush());

        mContainer.add(sPaintSurface, BorderLayout.CENTER);
        JPanel leftPanel = setupLeftPanel();
        mContainer.add(leftPanel, BorderLayout.LINE_START);

        sMainFrame.setVisible(true);
    }

    private void setupMenu() {

        JMenuBar menubar = new JMenuBar();
        JMenu menu1 = new JMenu("File");
        menu1.setMnemonic(KeyEvent.VK_F);
        menubar.add(menu1);
        sMainFrame.setJMenuBar(menubar);

        JMenuItem itemNew = new JMenuItem("New File");
        itemNew.setMnemonic(KeyEvent.VK_N);
        itemNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
        JMenuItem itemOpen = new JMenuItem("Open");
        itemOpen.setMnemonic(KeyEvent.VK_O);
        itemOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
        JMenuItem itemSaveAs = new JMenuItem("Save as...");
        itemSaveAs.setMnemonic(KeyEvent.VK_S);
        itemSaveAs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
        JMenuItem itemQuit = new JMenuItem("Quit");
        itemQuit.setMnemonic(KeyEvent.VK_Q);
        itemQuit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_DOWN_MASK));

        itemNew.addActionListener(action -> MenuActions.newFile(itemNew));
        itemSaveAs.addActionListener(action -> MenuActions.saveAs(itemSaveAs));
        itemOpen.addActionListener(action -> MenuActions.open(itemOpen));
        itemQuit.addActionListener(action -> MenuActions.quit(itemQuit));

        menu1.add(itemNew);
        menu1.add(itemOpen);
        menu1.add(itemSaveAs);
        menu1.add(itemQuit);
        menu1.addSeparator();
    }

    private JPanel setupLeftPanel() {
        JButton brush = new JButton("Brush");
        JButton line = new JButton("Line");
        JButton rect = new JButton("Rectangle");
        JButton circle = new JButton("Circle");
        JButton clear = new JButton("Clear");

        brush.addActionListener(action -> sPaintSurface.setFigure(Brush.getInstance()));
        line.addActionListener(action -> sPaintSurface.setFigure(Line.getInstance()));
        rect.addActionListener(e -> sPaintSurface.setFigure(Rectangle.getInstance()));
        circle.addActionListener(action -> sPaintSurface.setFigure(Circle.getInstance()));
        clear.addActionListener(action -> sPaintSurface.clear());

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(20, 20, 20, 20));
        panel.setLayout(new GridLayout(9, 1));

        borderColor = new ColorChooserButton(Color.BLACK, "■", ColorChooserButton.BORDER);
        borderColor.setForeground(Color.black);
        borderColor.setFont(new Font("Serif", Font.PLAIN, 36));


        fillColor = new ColorChooserButton(Color.BLACK, "■", ColorChooserButton.FILL);
        fillColor.setForeground(Color.black);
        fillColor.setFont(new Font("Serif", Font.PLAIN, 36));

        JSlider slider = new JSlider();
        slider.setMinimum(MINIMUM_STROKE);
        slider.setMaximum(MAXIMUM_STROKE);
        slider.setValue(MINIMUM_STROKE);

        Hashtable<Integer, JLabel> table = new Hashtable<>();
        table.put(MAXIMUM_STROKE/2, new JLabel(String.format("%2d  ",ParamPicker.getStroke())));
        slider.setPaintLabels(true);
        slider.setLabelTable(table);

        slider.addChangeListener(e -> {
            JSlider source = (JSlider) e.getSource();
                ParamPicker.setStroke(source.getValue());
                ((JLabel) slider.getLabelTable().get(MAXIMUM_STROKE/2))
                        .setText(String.format("%2d",ParamPicker.getStroke()));
        });
        slider.setPreferredSize(new Dimension(20, 20));




        JSlider alphaSlider = new JSlider();
        alphaSlider.setMinimum(MINIMUM_ALPHA);
        alphaSlider.setMaximum(MAXIMUM_ALPHA);
        alphaSlider.setValue(MAXIMUM_ALPHA);

        Hashtable<Integer, JLabel> alphaTable = new Hashtable<>();
        alphaTable.put(MAXIMUM_ALPHA/2, new JLabel(String.format("%d%%",alphaSlider.getValue())));
        alphaSlider.setPaintLabels(true);
        alphaSlider.setLabelTable(alphaTable);

        alphaSlider.addChangeListener(e -> {
            JSlider source = (JSlider) e.getSource();
            ParamPicker.setAlpha(source.getValue()/100f);
            ((JLabel) alphaSlider.getLabelTable().get(MAXIMUM_ALPHA/2))
                    .setText(String.format("%d%%",source.getValue()));
        });
        alphaSlider.setPreferredSize(new Dimension(20, 20));


        JPanel topPanel = new JPanel();
        topPanel.setLayout(new GridLayout(1, 2));
        topPanel.setBorder(new EmptyBorder(20,0,20,0));

        JButton undo = new JButton("U");
        JButton redo = new JButton("R");

        undo.getInputMap(JButton.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK), "undo");
        undo.getActionMap().put("undo", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                undo.doClick();
            }
        });


        redo.getInputMap(JButton.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK + InputEvent.ALT_MASK), "redo");
        redo.getActionMap().put("redo", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                redo.doClick();
            }
        });

        undo.addActionListener(action -> sPaintSurface.undo());
        redo.addActionListener(action -> sPaintSurface.redo());

        topPanel.add(undo);
        topPanel.add(redo);

        JPanel colorPanel = new JPanel(new GridLayout(2,1));
        JPanel colors = new JPanel(new GridLayout(1,2));
        JPanel colorLabels = new JPanel(new GridLayout(1,2));

        JLabel border = new JLabel("Border");
        border.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel fill = new JLabel("Fill");
        fill.setHorizontalAlignment(SwingConstants.CENTER);

        colorLabels.add(border);
        colorLabels.add(fill);

        colors.add(borderColor);
        colors.add(fillColor);

        colorPanel.add(colorLabels);
        colorPanel.add(colors);

        JPanel width = new JPanel(new GridLayout(2,1));
        JLabel widthLabel = new JLabel("Width");
        widthLabel.setHorizontalAlignment(SwingConstants.CENTER);
        width.add(widthLabel);
        width.add(slider);

        JPanel alpha = new JPanel(new GridLayout(2,1));
        JLabel alphaLabel = new JLabel("Alpha");
        alphaLabel.setHorizontalAlignment(SwingConstants.CENTER);
        alpha.add(alphaLabel);
        alpha.add(alphaSlider);

        panel.add(topPanel);
        panel.add(brush);
        panel.add(line);
        panel.add(rect);
        panel.add(circle);
        panel.add(clear);
        panel.add(colorPanel);
        panel.add(width);
        panel.add(alpha);

        return panel;
    }

    public static void setCurColor(Color color) {
            borderColor.setForeground(color);
    }


    public static void setCurFillColor(Color color) {
        fillColor.setForeground(color);
    }

    public static PaintSurface getPaintSurface() {
        return sPaintSurface;
    }
}