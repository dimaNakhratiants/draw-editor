import java.awt.*;

/**
 * Created by HP on 09.02.2017.
 */
public class ParamPicker {

    private static Color sColor = Color.black;
    private static Color sFillColor = Color.black;
    private static float sAlpha = 1;
    private static int sStroke = 1;
    private static Object image;

    public static int getStroke() {
        return sStroke;
    }

    public static void setStroke(int stroke) {
        sStroke = stroke;
    }

    public static Color getColor() {
        return sColor;
    }

    public static void setColor(Color color) {
        sColor = color;
        DrawEditor.setCurColor(color);
    }

    public static Color getFillColor() {
        return sFillColor;
    }

    public static void setFillColor(Color fillColor) {
        sFillColor = fillColor;
        DrawEditor.setCurFillColor(fillColor);
    }

    public static float getAlpha() {
        return sAlpha;
    }

    public static void setAlpha(float alpha) {
        sAlpha = alpha;
    }
}
